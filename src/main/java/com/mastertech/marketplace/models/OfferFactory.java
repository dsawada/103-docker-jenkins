package com.mastertech.marketplace.models;

public class OfferFactory {

	/**
	 * 
	 * @param prototype an incomple offer received from the client
	 * @return a complete offer, ready to be saved in the database
	 * @throws InvalidOffer 
	 */
	public Offer completeOffer(Offer offer) throws InvalidOffer {
		offer.setApproved(false);
		if (offer.getBid() < 0.5*offer.getProduct().getPrice()) {
			throw new InvalidOffer("Bid is too low");
		}
		return offer;
	}

	
}
